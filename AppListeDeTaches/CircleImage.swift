//
//  CircleImage.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    
    var body: some View {
       image
        .resizable()
        .clipShape(Circle())
        .overlay(Circle().stroke(Color.white, lineWidth: 4))
        .shadow(radius: 10)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage(image: Image("image1"))
    }
}
