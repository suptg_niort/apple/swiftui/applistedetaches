//
//  TaskRow.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 PRESENTATION D'UNE CELLULE D'UNE TACHE
 */

import SwiftUI

struct TaskRow: View {
    var task: Task
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            HStack {
                task.taskImage
                    .resizable()
                    .frame(width: 50, height: 50)
                
                Text(task.titre)
                    .bold()
                
                Spacer()
                
                Text(task.date)
                    .font(.headline)
                
                if task.status {
                   Image(systemName: "chevron.down.square.fill")
                    .imageScale(.medium)
                    .foregroundColor(.green)
                }
                
            }
                
            VStack(alignment: .leading) {
                
                Text(task.detail)
                .lineLimit(2)
                .font(.footnote)
                .foregroundColor(.gray)
                
            }
        }
    }
}

struct TaskRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TaskRow(task: taskData[0])
            TaskRow(task: taskData[1])
        }.previewLayout(.sizeThatFits)
    }
}
