//
//  TaskForm.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 FORMULAIRE DE CREATION D'UNE NOUVELLE TACHE
 */

import SwiftUI
import Combine

struct TaskForm: View {
    var task: Task
    @ObservedObject var userData = UserData()
    var taskIndex: Int {
           userData.task.firstIndex(where: { $0.titre == task.titre })!
       }
    
    @State var titre = ""
    @State var detail = ""
    @State var selectedDate = Date()
    var dateMin = Date(timeIntervalSinceNow: 0)
    var dateMax = Date(timeIntervalSinceNow: 63072000)
   
    
    var body: some View {
        NavigationView {
            VStack {
                
                Image("image1")
                    .resizable()
                    .scaledToFill()
                    .frame(height: 150)
                    .padding(-50)
                    .offset(x: 0, y: -100)
                    
                
                Form {
                    Section (header: Text("Titre : ")) {
                        
                        TextField("Saisir le titre de la tâche", text: $titre)
                        
                    }
                    
                    Section (header: Text("Description : ")) {
                        
                        TextField("Saisir la description de la tâche", text: $detail)
                        
                    }
                    
                    Section (header:  Text("Date : ")) {
                       
                        FormDatePicker(dateMin: dateMin, dateMax: dateMax, date: $selectedDate)
                        
                    }
                    
                    Section (header: Text("Status : ")) {
                        
                        
                        Button(action: {
                            self.userData.task[self.taskIndex].status.toggle()
                        }) {
                            if self.userData.task[self.taskIndex].status {
                                HStack {
                                    Text(" Réalisée ")
                                        .foregroundColor(Color.green)
                                    
                                    Spacer()
                                    
                                    Image(systemName: "chevron.down.square.fill")
                                        .foregroundColor(Color.green)
                                }
                                
                            } else {
                                HStack {
                                    Text(" Non Réalisée ")
                                        .foregroundColor(.red)
                                    
                                    Spacer()
                                    
                                    Image(systemName: "clear.fill")
                                        .foregroundColor(Color.red)
                                }
                            }
                        }
                    }
                     
                    //Affichage des champs cachés
                    if self.verificationInformations() {
                        Button(action: {
                            //Ajout de la nouvelle tâche à la liste de tâches
                            self.addNewTask()
                            
                        }) {
                            Text("Ajouter")
                        }
                    }
                }
            }
            .navigationBarTitle(Text("Nouvelle Tâche"))
        }
    }
    
    //Affichage du bouton que si les champs sont remplis
    private func verificationInformations() -> Bool {
        if titre.isEmpty { return false }
        
        if detail.isEmpty { return false }
        
        if dateToString().isEmpty { return false }
        
        return true
    }
    
   func addNewTask() {
        var newTask = task
        newTask.id = UUID().uuidString
        newTask.titre = self.titre
        newTask.detail = self.detail
        newTask.date = dateToString()
        newTask.status = false
        newTask.image = "image1"
        taskData.append(newTask)
    }
}

struct TaskForm_Previews: PreviewProvider {
    static var previews: some View {
        TaskForm(task: taskData[0]).environmentObject(UserData())
    }
}

extension TaskForm {
    
    //transformation du format Date() en chaine de caractères
    func dateToString() -> String {
         let formatter = DateFormatter()
         formatter.dateStyle = .short
         formatter.locale = Locale(identifier: "fr_FR")
         formatter.dateFormat = "dd-MM-yyyy"
         return formatter.string(from: selectedDate)
    }
}
