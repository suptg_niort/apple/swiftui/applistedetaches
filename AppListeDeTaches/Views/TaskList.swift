//
//  TaskList.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 LISTE DE TACHES
 */

import SwiftUI

struct TaskList: View {
    var task: Task
    @EnvironmentObject var userData: UserData 
    @State var showForm = false
    
    var body: some View {
        NavigationView {
            List {
                Toggle(isOn: $userData.showRealisedTaskOnly) {
                    
                    Text((self.userData.showRealisedTaskOnly) ? "Afficher les tâches réalisées " : "Afficher toutes les tâches ")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                
                ForEach(userData.task) { task in
                        if self.userData.showRealisedTaskOnly || task.status {
                            NavigationLink(destination: TaskDetail(task: task)) {
                                TaskRow(task: task)
                        }
                    }
                }
                .onDelete { (indexSet) in
                    //Si récupération d'un index, on peut supprimer
                    if let index = indexSet.first {
                        self.userData.task.remove(at: index)
                    }
                }
                .onMove(perform: moveTask)
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle(Text("Liste de tâches"))
            .navigationBarItems(
                leading: EditButton(),
                trailing: Button(action: {
                    
                    self.showForm.toggle()
    
                }) {
                    Text("Ajouter")
                })
            .sheet(isPresented: $showForm) {
                TaskForm(task: taskData[0])
            }
        }
    }
    
    //Changer la disposition des lignes à la liste de tâches
    private func moveTask(from indexes:IndexSet,to destination:Int) {
        taskData.move(fromOffsets: indexes, toOffset: destination)
    }
    
}

struct TaskList_Previews: PreviewProvider {
    static var previews: some View {
        TaskList(task: taskData[0]).environmentObject(UserData())
    }
}
