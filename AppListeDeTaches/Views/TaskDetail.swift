//
//  TaskDetail.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 PRESENTATION D'UNE VUE DETAILLEE D'UNE TACHE
 */

import SwiftUI

struct TaskDetail: View {
    var task: Task
    @EnvironmentObject var userData: UserData
    
    var taskIndex: Int {
        userData.task.firstIndex(where: { $0.titre == task.titre })!
    }
    
    var body: some View {
        
        VStack {
            task.taskImage
                .resizable()
                .scaledToFill()
                .frame(height: 150)
                .clipped()
                .blur(radius: 7, opaque: true)
            
            CircleImage(image: task.taskImage)
                .frame(width: 150, height: 150)
                .offset(x: 90, y: -90)
                .padding(.bottom, 5)

            VStack(alignment: .leading, spacing: 10) {
                
                Text(task.titre)
                    .font(.title)
                
                Text(task.date)
                .font(.headline)
                
                Button(action: {
                    self.userData.task[self.taskIndex].status.toggle()
                }) {
                    if self.userData.task[self.taskIndex].status {
                        Text(" Réalisée ")
                            .foregroundColor(Color.green)
                        
                        Spacer()
                        
                        Image(systemName: "chevron.down.square.fill")
                            .foregroundColor(Color.green)
                    } else {
                        Text(" Non Réalisée ")
                            .foregroundColor(.red)
                        
                        Spacer()
                        
                        Image(systemName: "clear.fill")
                            .foregroundColor(Color.red)
                    }
                }
                
                Divider()
                
                Text(task.detail)
                    .font(.subheadline)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.leading)
                    .lineLimit(nil)
                
                Divider()
                
                Spacer()

            }.offset(x: 0, y: -140)
            .padding()
        }
        .navigationBarTitle(Text(task.titre), displayMode: .inline)
    }
}

struct TaskDetail_Previews: PreviewProvider {
    static var previews: some View {
        TaskDetail(task: taskData[0]).environmentObject(UserData())
    }
}
