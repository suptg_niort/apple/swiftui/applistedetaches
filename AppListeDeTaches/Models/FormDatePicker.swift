//
//  DatePicker.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 Sélecteur de date personnalisé qui démarre à la date du jour
 */

import Foundation
import SwiftUI

struct FormDatePicker: View {
    
    var dateMin: Date
    var dateMax: Date
    @Binding var date : Date
    
    var body: some View {
        
        DatePicker(selection: $date,in: dateMin...dateMax ,displayedComponents: .date) {
            
            Text("Date sélectionnée : ")
                .foregroundColor(.gray)

        }
    }
        
    //transformation du format Date() en chaine de caractères
    func dateToString() -> String {
         let formatter = DateFormatter()
         formatter.dateStyle = .short
         formatter.locale = Locale(identifier: "fr_FR")
         formatter.dateFormat = "dd-MM-yyyy"
         return formatter.string(from: date)
    }
}
    
