//
//  Task.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 17/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 Modèle d'une tache individuelle
 */

import Foundation
import SwiftUI

struct Task: Hashable, Codable, Identifiable {
    
    var id: String
    var titre: String
    var detail: String
    var date: String
    var status: Bool
    var image: String

    
    //Constructeur avec une image et un status par défaut
    init(withParams id: UUID, titre: String, detail: String, date: String) {
        self.id = id.uuidString
        self.titre = titre
        self.detail = detail
        self.date = date
        self.status = false
        self.image = "image1"
    }
}

extension Task {
    var taskImage: Image {
        ImageStore.shared.image(name: image)
    }
}


