//
//  UserData.swift
//  AppListeDeTaches
//
//  Created by Enrick Enet on 20/04/2020.
//  Copyright © 2020 Enrick. All rights reserved.
//

/*
 Stockage des données 'task' dans un objet observable.
 - Objet personnalisé pour les données lié à une vue
 - Affichage correcte de la vue après une modification
 */

import Foundation
import Combine

final class UserData: ObservableObject {
    
    //Ajout des propriétés stockées & des valeurs initiales
    @Published var showRealisedTaskOnly = true
    @Published var task = taskData.sorted(by: {$0.date.compare($1.date) == .orderedDescending})
    
}
